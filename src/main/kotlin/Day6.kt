fun String.allUnique(): Boolean = all(hashSetOf<Char>()::add)

fun getNUniqueSequenceEnd(input: String, substringLength: Int): Int {
    var characterNumber = 0
    for (c in input.indices) {
        val currentCharacters = input.substring(c, c + substringLength)
        if (currentCharacters.allUnique()) {
            characterNumber = c + substringLength
            break
        }
    }
    return characterNumber
}

fun day6Part1(input: List<String>): Int {
    return getNUniqueSequenceEnd(input[0], 4)
}

fun day6Part2(input: List<String>): Int {
    return getNUniqueSequenceEnd(input[0], 14)
}
