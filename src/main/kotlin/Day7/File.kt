package Day7

class File(
    private val name: String,
    private val size: Int,
) {
    fun getFileSize(): Int {
        return size
    }

    override fun toString(): String {
        return "File(name='$name', size=$size)"
    }
}
