package Day7

class Directory(
    private val name: String, private val parentDirectory: Directory?
) {

    private var files: MutableList<File> = mutableListOf()
    private var directories: MutableList<Directory> = mutableListOf()

    fun getTotalFileSize(): Int {
        var totalFileSize = 0

        for (f in files) {
            totalFileSize += f.getFileSize()
        }
        for (d in directories) {
            totalFileSize += d.getTotalFileSize()
        }
        return totalFileSize
    }

    fun getAllSmallDirectories(): List<Directory> {
        val directoryList: MutableList<Directory> = mutableListOf()
        if (directories.isNotEmpty()) {
            for (d in directories) {
                directoryList.addAll(d.getAllSmallDirectories())
            }
        }
        if (this.getTotalFileSize() <= 100000) {
            directoryList.add(this)
        }
        return directoryList
    }

    fun getAllDirectories(): List<Directory> {
        val directoryList: MutableList<Directory> = mutableListOf()
        if (directories.isNotEmpty()) {
            for (d in directories) {
                directoryList.addAll(d.getAllDirectories())
            }
        }
        directoryList.add(this)
        return directoryList
    }

    fun getName(): String {
        return name
    }

    fun addDirectory(directory: Directory) {
        directories.add(directory)
    }

    fun getParentDirectory(): Directory {
        return parentDirectory!!
    }

    fun enterDirectory(dirName: String): Directory? {
        return directories.find { dirName == it.getName() }

    }

    fun addFile(file: File) {
        files.add(file)
    }

    override fun toString(): String {
        return "Directory(name='$name', parentDirectory=$parentDirectory)"
    }
}
