package Day7

fun getInputIntoDirectories(input: List<String>): Directory {
    val rootDirectory = Directory("/", null)
    var currentDirectory = rootDirectory

    for (l in input.indices) {
        if (input[l][0] == '$') {
            if (input[l].split(' ')[1] == "cd") {
                currentDirectory = if (input[l].split(' ')[2] == "..") {
                    currentDirectory.getParentDirectory()
                } else {
                    currentDirectory.enterDirectory(input[l].split(' ')[2])!!
                }
            }
        } else if (input[l].split(' ')[0] == "dir") {
            currentDirectory.addDirectory(Directory(input[l].split(' ')[1], currentDirectory))
        } else {
            val file = input[l].split(' ')[1]
            val fileSize = input[l].split(' ')[0].toInt()
            currentDirectory.addFile(File(file, fileSize))
        }
    }
    return rootDirectory
}

fun day7Part1(input: List<String>): Int {

    val rootDirectory = getInputIntoDirectories(input.subList(1, input.size))

    val smallDirectories = rootDirectory.getAllSmallDirectories()
    var totalSmallDirectorySize = 0
    for (d in smallDirectories) {
        totalSmallDirectorySize += d.getTotalFileSize()
    }

    return totalSmallDirectorySize
}

fun day7Part2(input: List<String>): Int {
    val rootDirectory = getInputIntoDirectories(input.subList(1, input.size))

    val allDirectories = rootDirectory.getAllDirectories()
    val usedSpace = rootDirectory.getTotalFileSize()
    val availableSpace = 70000000 - usedSpace
    val neededSpace = 30000000 - availableSpace

    var lowestDirectorySize = rootDirectory.getTotalFileSize()
    for (d in allDirectories) {
        val currentDirectorySize = d.getTotalFileSize()
        if (currentDirectorySize >= neededSpace) {
            if ((currentDirectorySize - neededSpace) < (lowestDirectorySize - neededSpace)) {
                lowestDirectorySize = currentDirectorySize
            }
        }
    }

    return lowestDirectorySize
}
