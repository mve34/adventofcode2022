fun day3Part1(input: List<String>): Int {
    var totalValue = 0
    for (i in input) {
        val characterArray = i.toCharArray()
        val n = characterArray.size
        val compartment1 = characterArray.copyOfRange(0, (n + 1) / 2)
        val compartment2 = characterArray.copyOfRange((n + 1) / 2, n)
        for (character in compartment1) {
            if (compartment2.contains(character)) {
                totalValue += if (character.isUpperCase()) {
                    (character.code - 38)
                } else {
                    (character.code - 96)
                }
                break
            }
        }
    }
    return totalValue
}

fun day3Part2(input: List<String>): Int {
    var workableInput = input
    var totalValue = 0
    while (workableInput.isNotEmpty()) {
        val currentGroup = (workableInput.take(3))
        for (character in currentGroup[0]) {
            if (currentGroup[1].contains(character)) {
                if (currentGroup[2].contains(character)) {
                    totalValue += if (character.isUpperCase()) {
                        (character.code - 38)
                    } else {
                        (character.code - 96)
                    }
                    break
                }
            }
        }
        workableInput = workableInput.drop(3)
    }
    return totalValue
}
