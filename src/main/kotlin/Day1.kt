fun day1Part1(input: List<String>): Int {
    var maxTotal = 0
    var total = 0
    for (i in input) {
        if (total > maxTotal) maxTotal = total
        if (i == "") {
            total = 0
            continue
        }
        total += i.toInt()
    }
    return maxTotal
}

fun day1Part2(input: List<String>): Int {
    val totalList = mutableListOf<Int>()
    var total = 0
    for (i in input) {
        if (i == "") {
            totalList.add(total)
            total = 0
            continue
        }
        total += i.toInt()
    }
    totalList.add(total)

    totalList.sortDescending()

    return totalList[0] + totalList[1] + totalList[2]
}
