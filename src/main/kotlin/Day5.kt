fun inputToStacks(input: List<String>): MutableList<ArrayDeque<Char>> {
    var stacks = emptyList<String>().toMutableList()

    for (i in input.indices) {
        if (input[i] == "") {
            stacks = input.subList(0, i).toMutableList()
        }
    }

    for (i in stacks.indices) {
        if (stacks[i].contains("   ")) {
            if (stacks[i][0] == ' ') {
                stacks[i] = stacks[i].replace("    ", "[.] ")
            } else stacks[i] = stacks[i].replace("    ", " [.]")
        }
    }

    val newStacks: MutableList<List<String>> = stacks.map {
        it.split(" ")
    }.toMutableList()

    val numberOfStacks = newStacks.removeLast().filter { it.isNotEmpty() }.size
    val stackList = listOf<ArrayDeque<Char>>().toMutableList()

    for (i in 1..numberOfStacks) stackList.add(ArrayDeque())

    for (row in newStacks.indices) {
        for (column in newStacks[row].indices) {
            val currentCharacter = newStacks[row][column].replace("[", "").replace("]", "").toCharArray()[0]
            if (currentCharacter == '.') continue
            stackList[column].add(currentCharacter)
        }
    }

    return stackList
}

fun inputToActions(input: List<String>): List<List<String>> {
    var actions = emptyList<String>().toMutableList()

    for (i in input.indices) {
        if (input[i] == "") {
            actions = input.subList(i + 1, input.size).toMutableList()
        }
    }

    return actions.map { it.split(" ") }
}

fun day5Part1(input: List<String>): String {

    val stackList = inputToStacks(input)
    val actions = inputToActions(input)

    for (action in actions) {
        for (i in 1..action[1].toInt()) {
            val toMove = stackList[action[3].toInt() - 1].removeFirst()
            stackList[action[5].toInt() - 1].addFirst(toMove)
        }
    }

    var resultString = ""

    for (i in stackList) {
        resultString += i.first()
    }

    return resultString
}

fun day5Part2(input: List<String>): String {

    val stackList = inputToStacks(input)
    val actions = inputToActions(input)

    for (action in actions) {
        val tempStack = ArrayDeque<Char>()
        for (i in 1..action[1].toInt()) {
            tempStack.addFirst(stackList[action[3].toInt() - 1].removeFirst())
        }
        for (i in tempStack) {
            stackList[action[5].toInt() - 1].addFirst(i)
        }
    }

    var resultString = ""

    for (i in stackList) {
        resultString += i.first()
    }

    return resultString
}
