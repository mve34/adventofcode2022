fun day4Part1(input: List<String>): Int {
    val newInput = input.map { it.split(",") }
    var totalDuplicates = 0

    for (duo in newInput) {
        val firstRangeLow = duo[0].split("-")[0].toInt()
        val firstRangeHigh = duo[0].split("-")[1].toInt()
        val secondRangeLow = duo[1].split("-")[0].toInt()
        val secondRangeHigh = duo[1].split("-")[1].toInt()

        if ((firstRangeLow in secondRangeLow until (secondRangeHigh + 1) && firstRangeHigh in secondRangeLow until (secondRangeHigh + 1)) ||
            (secondRangeLow in firstRangeLow until (firstRangeHigh + 1) && secondRangeHigh in firstRangeLow until (firstRangeHigh + 1))
        ) {
            totalDuplicates++
        }
    }
    return totalDuplicates
}

fun day4Part2(input: List<String>): Int {
    val newInput = input.map { it.split(",") }
    var totalDuplicates = 0

    for (duo in newInput) {
        val firstRangeLow = duo[0].split("-")[0].toInt()
        val firstRangeHigh = duo[0].split("-")[1].toInt()
        val secondRangeLow = duo[1].split("-")[0].toInt()
        val secondRangeHigh = duo[1].split("-")[1].toInt()

        if (
            firstRangeLow in secondRangeLow until (secondRangeHigh + 1) ||
            firstRangeHigh in secondRangeLow until (secondRangeHigh + 1) ||
            secondRangeLow in firstRangeLow until (firstRangeHigh + 1) ||
            secondRangeHigh in firstRangeLow until (firstRangeHigh + 1)
        ) {
            totalDuplicates++
        }
    }
    return totalDuplicates
}
