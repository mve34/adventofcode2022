fun day2Part1(input: List<String>): Int {
    val newInput = input.map { string -> string.split(" ") }
    var totalScore = 0
    for (i in newInput) {
        var score = 0

        when (i[1]) {
            "X" -> {
                score += 1
                score += when (i[0]) {
                    "B" -> 0
                    "A" -> 3
                    else -> 6
                }
            }

            "Y" -> {
                score += 2
                score += when (i[0]) {
                    "C" -> 0
                    "B" -> 3
                    else -> 6
                }
            }

            "Z" -> {
                score += 3
                score += when (i[0]) {
                    "A" -> 0
                    "C" -> 3
                    else -> 6
                }
            }
        }
        totalScore += score
    }
    return totalScore
}

fun day2Part2(input: List<String>): Int {
    val newInput = input.map { string -> string.split(" ") }
    var totalScore = 0
    for (i in newInput) {
        var score = 0

        when (i[1]) {
            // lose
            "X" -> {
                score += when (i[0]) {
                    "A" -> 3
                    "B" -> 1
                    else -> 2
                }
            }
            // draw
            "Y" -> {
                score += 3
                score += when (i[0]) {
                    "A" -> 1
                    "B" -> 2
                    else -> 3
                }
            }
            // win
            "Z" -> {
                score += 6
                score += when (i[0]) {
                    "A" -> 2
                    "B" -> 3
                    else -> 1
                }
            }
        }
        totalScore += score
    }
    return totalScore
}
