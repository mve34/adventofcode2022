import Day7.day7Part1
import Day7.day7Part2
import Day7.getInputIntoDirectories

fun main() {

    fun day8Part1(input: List<String>): Int {
        return 0
    }

    fun day8Part2(input: List<String>): Int {
        return 0
    }

//    val day1TestInput = readInput("main/resources/Day1/Day01_test")
//    check(day1Part1(day1TestInput) == 24000)
//    check(day1Part2(day1TestInput) == 45000)
//
//    val day1Input = readInput("main/resources/Day1/Day01")
//    println("\nDay1 of part1: " + day1Part1(day1Input))
//    println("Day1 of part2: " + day1Part2(day1Input))
//
//    val day2TestInput = readInput("main/resources/Day2/Day02_test")
//    check(day2Part1(day2TestInput) == 15)
//    check(day2Part2(day2TestInput) == 12)
//
//    val day2Input = readInput("main/resources/Day2/Day02")
//    println("\nDay2 of part1: " + day2Part1(day2Input))
//    println("Day2 of part2: " + day2Part2(day2Input))
//
//    val day3TestInput = readInput("main/resources/Day3/Day03_test")
//    check(day3Part1(day3TestInput) == 157)
//    check(day3Part2(day3TestInput) == 70)
//
//    val day3Input = readInput("main/resources/Day3/Day03")
//    println("\nDay3 of part1: " + day3Part1(day3Input))
//    println("Day3 of part2: " + day3Part2(day3Input))
//
//    val day4TestInput = readInput("main/resources/Day4/Day04_test")
//    check(day4Part1(day4TestInput) == 2)
//    check(day4Part2(day4TestInput) == 4)
//
//    val day4Input = readInput("main/resources/Day4/Day04")
//    println("\nDay4 of part1: " + day4Part1(day4Input))
//    println("Day4 of part2: " + day4Part2(day4Input))
//
//    val day5TestInput = readInput("main/resources/Day5/Day05_test")
//    check(day5Part1(day5TestInput) == "CMZ")
//    check(day5Part2(day5TestInput) == "MCD")
//
//    val day5Input = readInput("main/resources/Day5/Day05")
//    println("\nDay5 of part1: " + day5Part1(day5Input))
//    println("Day5 of part2: " + day5Part2(day5Input))
//
//    val day6TestInput = readInput("main/resources/Day6/Day06_test")
//    check(day6Part1(day6TestInput) == 7)
//    check(day6Part2(day6TestInput) == 19)
//
//    val day6Input = readInput("main/resources/Day6/Day06")
//    println("\nDay6 of part1: " + day6Part1(day6Input))
//    println("Day6 of part2: " + day6Part2(day6Input))

//    val day7TestInput = readInput("main/resources/Day7/Day07_test")
//    check(day7Part1(day7TestInput) == 95437)
//    check(day7Part2(day7TestInput) == 24933642)
//
//    val day7Input = readInput("main/resources/Day7/Day07")
//    println("\nDay7 of part1: " + day7Part1(day7Input))
//    println("Day7 of part2: " + day7Part2(day7Input))

    val day8TestInput = readInput("main/resources/Day8/Day08_test")
    check(day8Part1(day8TestInput) == 0)
    check(day8Part2(day8TestInput) == 0)

    val day8Input = readInput("main/resources/Day8/Day08")
    println("\nDay8 of part1: " + day8Part1(day8Input))
    println("Day8 of part2: " + day8Part2(day8Input))
}
